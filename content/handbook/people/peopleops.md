---
title: PeopleOps
description:
  This page describes our people-oriented processes and important concepts such
  as DRIs and how to get employment verification letters.
---

# {{% param "title" %}}

{{% param "description" %}}

## Directly responsible individuals

At Synura we use the concept of directly responsible individuals (DRIs), a
person who is singularly responsible for a given aspect of the open source
project, the product, or the company. This person is responsible for
accomplishing goals and making decisions about a particular aspect of Synura.
DRIs help us collaborate efficiently by knowing exactly who is responsible, and
can make decisions about the work they're doing.

- Once DRIs are identified for different parts of our platform (this is still
  TBD), they will be documented here.

You can read more about directly responsible individuals in
[Gitlab's handbook](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/)

## Security

In case of lost or stolen property Employee should immediately notify
security@synura.com.

## Letters of employment & full-time contractor

Please submit your Letter of Employment / Full-Time Contractor to
peopleops@synura.com with the following information:

- Example Letter from the entity to be presented to (Financial Establishment,
  School, etc.) or complete the template here (document TBD).

Your request will be reviewed and returned to you within 72hs of submission.
