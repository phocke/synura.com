---
title: Jason's README
description:
  Hello, I’m Jason Yavorska and I’m the founding PM (and acting CEO) for Synura!
  On this page you can learn about my work style so we can work together well.
---

# {{% param "title" %}}

{{% param "description" %}}

## How and when to get my help

I'm here to build a great team and help us all be as effective as possible. This
[Twitter thread](https://twitter.com/jmwind/status/1493569303030816770)
summarizes how I think about this very nicely and I'd encourage you to read it
if you report to me. A few other things I can help with:

- What I'll spend the much of my time on is connecting dots, alignment, making
  sure we collaborate efficiently, and otherwise building/hiring a great group
  of people.
- I can jump in and help if you're feeling overloaded. I just need you to
  clearly ask (I won't show up and take over uninvited).
- I tend to be good at unblocking, helping break down problems into iterations,
  and generally helping move things forward.

In general, when working with me I always appreciate direct feedback so don't
worry about upsetting me and feel free to share bad news early. I'm also more
likely to be optimistic than not, with a plan that we'll figure it out what we
need to when we need to, so feel free bring me back to reality. Finally, I tend
to debate different perspectives to solve problems, including playing "Devil's
Advocate". If that puts you off that's fine—just tell me and we’ll use another
approach.

## Reference

These are some of my favorite product principles.

- I love thinking about technology, processes, personalities, and efficiency,
  but what really drives me is what Marc Andreessen described as
  [the one thing that matters](https://pmarchive.com/guide_to_startups_part4.html):
  the journey of finding and growing product market fit.
- I like to follow the
  [reasonable person principle](http://www.cs.cmu.edu/~weigand/staff/).
- I have learned that working on things
  [iteratively](https://about.gitlab.com/handbook/values/#iteration) tends to
  get the best results.
- Talking to customers in a way where they tell you the truth about what they
  actually are willing to pay for is a subtle skill. This
  [video](https://www.youtube.com/watch?v=MT4Ig2uqjTc) and
  [book](http://momtestbook.com/) can help you learn to do that better.
- Organizations really are
  [like slime molds](https://komoroske.com/slime-mold/). This ties into this
  [Twitter thread](https://twitter.com/jmwind/status/1493569303030816770) also
  linked above on why alignment is so important.

## Management Style

My own default approach to management is:

- Non-hierarchical: the best way to solve a conflict is by getting the people
  together who have the problem, rather than finding the common manager of them
  and having them make a decision. One exception is conflicts of prioritization
  between teams, which I can help.
- Servant leadership: I avoid making promises on your behalf, and give you the
  opportunity to speak for yourself for your area. My role as your coach is to
  help you improve in your leadership, decision making, and role-based skills,
  and to be someone to bounce ideas off of.
- Team oriented: My main job is to create a great team, with great leaders, who
  make great decisions and excel at their jobs. It’s not for me to build a team
  of “assistants” who enable me to be able to know and do everything. That
  doesn’t scale, is demotivating, and creates a single point of failure for the
  company. Remember that I don’t expect perfection out of anyone, and I can help
  you learn to do this well.
- No ego: I won't defend a point to win an argument or double-down on a mistake.
  Honest, direct feedback is always welcome and is always available. It’s also
  simply not possible to “step on my toes” at work: you can always come to me
  with your thoughts and opinions and we’ll work together, without blame or
  defensiveness, to handle them as best we can.

## Personal Style

My
[Enneagram type is The Enthusiast](https://www.enneagraminstitute.com/type-7),
and my StrengthsFinder types are
[Strategic](https://strengthsschool.com/strategic-strengthsfinder),
[Relator](https://strengthsschool.com/relator-strengthsfinder),
[Adaptability](https://strengthsschool.com/adaptability-strengthsfinder),
[Maximizer](https://strengthsschool.com/maximizer-strengthsfinder) and
[Futuristic](https://strengthsschool.com/futuristic-strengthsfinder). For the
Insights personality test, I’m a “Creative Directing Motivator”. I tend to have
a creative and intuitive way of working, which works really well with iteration.
It also means that I really enjoy sharing my enthusiasm and passion for the
problems I am working on, and that I excel in unstructured, fast-moving
environments. I'm motivated by being appreciated and working on innovative
solutions. A sample motto for an expressive manager like me might be "I lean on
the power of my vision, and trust in my persuasive power."
