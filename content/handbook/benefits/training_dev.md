---
title: Training & development
description:
  This page describes resources you have available to learn and grow. Note that
  this is a list of examples, but not necessarily comprehensive.
---

# {{% param "title" %}}

{{% param "description" %}}

If you have ideas for more kinds of development resources, or if you need
something unique for yourself, reach out to your manager to discuss.

## Courses

Synura encourages team members to invest in their personal growth. Our official
programs for this are still TBD, but in the meantime teammates may spend up to
$600 USD per year on other courses or professional certifications, so long as
they are discussed with their manager in advance, they have the time available
while fulfilling their work responsibilities, and relevant to their Synura
responsibilities.
